import 'package:flutter/material.dart';

class SpaceSizedBox extends StatelessWidget {
  const SpaceSizedBox({super.key, required this.height});

  final double height;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
    );
  }
}
