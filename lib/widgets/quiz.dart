import 'package:flutter/material.dart';
import 'package:flutter_quizz_app/data/questions.dart';
import 'package:flutter_quizz_app/screens/home_screen.dart';
import 'package:flutter_quizz_app/screens/question_screen.dart';
import 'package:flutter_quizz_app/screens/result_screen.dart';

class Quiz extends StatefulWidget {
  const Quiz({super.key});

  @override
  State<Quiz> createState() => _QuizState();
}

class _QuizState extends State<Quiz> {
  List<String> selectedAnswers = [];
  String activeScreen = 'start-screen';

  void switchScreen() {
    setState(() {
      activeScreen = "questions-screens";
    });
  }

  void chooseAnswer(String answer) {
    selectedAnswers.add(answer);
    if (selectedAnswers.length == questions.length) {
      setState(() {
        activeScreen = 'result-screen';
      });
    }
  }

  void restartQuiz() {
    setState(() {
      selectedAnswers = [];
      activeScreen = 'start-screen';
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget screenWidget = HomeScreen(startQuiz: switchScreen);
    if (activeScreen == "questions-screens") {
      screenWidget = QuestionScreen(
        onSelectAnswer: chooseAnswer,
      );
    }

    if (activeScreen == "result-screen") {
      screenWidget = ResultScreen(
        restartQuiz: restartQuiz,
        chosenAnswers: selectedAnswers,
      );
    }

    return Scaffold(
      backgroundColor: Colors.amber,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        title: const Text("Quizz App"),
      ),
      body: Container(
        child: screenWidget,
      ),
    );
  }
}
