import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_quizz_app/widgets/space_sizedbox.dart';
import 'package:google_fonts/google_fonts.dart';

class QuestionsSummary extends StatelessWidget {
  const QuestionsSummary({super.key, required this.summaryData});

  final List<Map<String, Object>> summaryData;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 400,
      child: SingleChildScrollView(
        child: Column(
          children: summaryData.map(
            (item) {
              return Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: 30,
                    height: 30,
                    margin: const EdgeInsets.only(right: 20),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        color: item["correct_answer"] == item["user_answer"]
                            ? Colors.green
                            : Colors.red,
                        borderRadius: BorderRadius.circular(100)),
                    child: Text(
                      ((item['question_index'] as int) + 1).toString(),
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  const SpaceSizedBox(height: 20),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          item['question'].toString(),
                          style: GoogleFonts.lato(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        const SpaceSizedBox(height: 5),
                        Text(
                          item['user_answer'].toString(),
                          style: const TextStyle(color: Colors.black54),
                        ),
                        Text(
                          item['correct_answer'].toString(),
                          style: const TextStyle(
                              color: Color.fromRGBO(76, 175, 80, 1)),
                        ),
                        const SpaceSizedBox(height: 10)
                      ],
                    ),
                  ),
                ],
              );
            },
          ).toList(),
        ),
      ),
    );
  }
}
