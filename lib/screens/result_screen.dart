import 'package:flutter/material.dart';
import 'package:flutter_quizz_app/data/questions.dart';
import 'package:flutter_quizz_app/widgets/questions_summary.dart';
import 'package:flutter_quizz_app/widgets/space_sizedbox.dart';
import 'package:google_fonts/google_fonts.dart';

class ResultScreen extends StatelessWidget {
  const ResultScreen(
      {super.key, required this.restartQuiz, required this.chosenAnswers});

  final void Function() restartQuiz;
  final List<String> chosenAnswers;

  List<Map<String, Object>> get summaryData {
    final List<Map<String, Object>> summary = [];

    for (var i = 0; i < chosenAnswers.length; i++) {
      var question = questions[i];
      var chosenAnswer = chosenAnswers[i];
      summary.add({
        'question_index': i,
        'question': question.text,
        'user_answer': chosenAnswer,
        'correct_answer': question.answers[0]
      });
    }

    return summary;
  }

  @override
  Widget build(BuildContext context) {
    final numTotalQuestions = questions.length;
    final numCorrectQuestions = summaryData
        .where((element) => element["correct_answer"] == element["user_answer"])
        .length;

    return SizedBox(
      width: double.infinity,
      child: Container(
        margin: const EdgeInsets.all(40),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "You answer $numCorrectQuestions out of $numTotalQuestions questions correctly !",
              style: GoogleFonts.lato(
                color: Colors.white,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SpaceSizedBox(
              height: 30,
            ),
            QuestionsSummary(summaryData: summaryData),
            const SpaceSizedBox(height: 30),
            TextButton.icon(
              onPressed: () {
                restartQuiz();
              },
              label: const Text(
                "Restart Quiz!",
              ),
              icon: const Icon(Icons.restart_alt),
              style: TextButton.styleFrom(
                  backgroundColor: Colors.white,
                  foregroundColor: Colors.redAccent,
                  textStyle: const TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  )),
            )
          ],
        ),
      ),
    );
  }
}
