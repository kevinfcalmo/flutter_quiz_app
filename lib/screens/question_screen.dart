import 'package:flutter/material.dart';
import 'package:flutter_quizz_app/data/questions.dart';
import 'package:flutter_quizz_app/widgets/answer_button.dart';
import 'package:flutter_quizz_app/widgets/space_sizedbox.dart';
import 'package:google_fonts/google_fonts.dart';

class QuestionScreen extends StatefulWidget {
  const QuestionScreen({super.key, required this.onSelectAnswer});

  final void Function(String answer) onSelectAnswer;

  @override
  State<QuestionScreen> createState() => _QuestionScreenState();
}

class _QuestionScreenState extends State<QuestionScreen> {
  var currentQuestionIndex = 0;

  void answerQuestion(String selectedAnswers) {
    widget.onSelectAnswer(selectedAnswers);
    setState(() {
      currentQuestionIndex++;
    });
  }

  @override
  Widget build(BuildContext context) {
    final currentQuestion = questions[currentQuestionIndex];
    return SizedBox(
      width: double.infinity,
      child: Container(
        margin: const EdgeInsets.all(40),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              currentQuestion.text,
              textAlign: TextAlign.center,
              style: GoogleFonts.lato(
                  color: Colors.red, fontSize: 24, fontWeight: FontWeight.bold),
            ),
            const SpaceSizedBox(height: 30),
            ...currentQuestion.getShuffledAnswers().map((anwser) {
              return Container(
                margin: const EdgeInsets.symmetric(vertical: 5),
                child: AnswerButton(
                    answerText: anwser,
                    onTap: () {
                      answerQuestion(anwser);
                    }),
              );
            }),
          ],
        ),
      ),
    );
  }
}
