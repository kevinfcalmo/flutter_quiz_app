import 'package:flutter/material.dart';
import 'package:flutter_quizz_app/widgets/space_sizedbox.dart';
import 'package:google_fonts/google_fonts.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key, required this.startQuiz});

  final void Function() startQuiz;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image.asset(
            "assets/images/quiz-logo.png",
            width: 300,
            //rendre l'image transparente
            color: const Color.fromARGB(149, 255, 255, 255),
          ),
          const SpaceSizedBox(
            height: 80,
          ),
          Text(
            "Learn Flutter the fun way!",
            style: GoogleFonts.lato(color: Colors.white),
          ),
          const SpaceSizedBox(
            height: 30,
          ),
          OutlinedButton.icon(
            icon: const Icon(
              Icons.arrow_right_alt,
            ),
            label: const Text("Start Quizz"),
            onPressed: startQuiz,
            style: OutlinedButton.styleFrom(
              foregroundColor: Colors.red,
              backgroundColor: Colors.white,
            ),
          )
        ],
      ),
    );
  }
}
