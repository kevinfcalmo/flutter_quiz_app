class QuizQuestion {
  QuizQuestion(this.text, this.answers);
  final String text;
  final List<String> answers;

  List<String> getShuffledAnswers() {
    // copie la liste de réponses
    final shuffledList = List.of(answers);
    // renvoie les réponses de façon aléatoire
    shuffledList.shuffle();
    return shuffledList;
  }
}
